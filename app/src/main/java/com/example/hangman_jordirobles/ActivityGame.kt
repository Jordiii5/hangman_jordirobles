package com.example.hangman_jordirobles
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.example.hangman_jordirobles.databinding.GameScreenBinding

class ActivityGame : AppCompatActivity(), View.OnClickListener {
    lateinit var binding: GameScreenBinding
    val easy = listOf("CATAR", "ECUADOR", "SENEGAL", "PAÍSES/BAJOS", "INGLATERRA", "IRAN", "ESTADOS/UNIDOS", "GALES", "ARGENTINA", "ARABIA/SAUDI", "MEXICO", "POLONIA", "FRANCIA", "AUSTRALIA", "DINAMARCA", "TUNEZ", "ESPAÑA", "COSTA/RICA", "ALEMANIA", "JAPON", "BELGICA", "CANADA", "MARRUECOS", "CROACIA", "BRASIL", "SERBIA", "SUIZA", "CAMERUN", "PORTUGAL", "GHANA", "URUGUAY", "COREA/DEL/SUR")
    val medium = listOf("URUGUAY", "MEXICO", "SUDAFRICA", "FRANCIA", "ARGENTINA", "COREA/DEL/SUR", "GRECIA", "NIGERIA", "ESTADOS/UNIDOS", "INGLATERRA", "ESLOVENIA", "ARGELIA", "ALEMANIA", "GHANA", "AUSTRALIA", "SERBIA", "PAISES/BAJOS", "JAPON", "DINAMARCA", "CAMERUN", "PARAGUAY", "ESLOVAQUIA", "NUEVA/ZELANDA", "ITALIA", "BRASIL", "PORTUGAL", "COSTA/DE/MARFIL", "COREA/DEL/NORTE", "ESPAÑA", "CHILE", "SUIZA", "HONDURAS")
    val hard = listOf("ARGENTINA", "CHILE", "MEXICO", "RUMANIA", "BELGICA", "ESTADOS/UNIDOS", "PARAGUAY", "URUGUAY", "BOLIVIA", "FRANCIA", "PERU", "YUGOSLAVIA", "BRASIL")
    lateinit var words: List<String>
    lateinit var wordrandom: String
    var wordMessage = ""
    val listOfWordMessage = mutableListOf<String>()
    var attempts = 0
    val hangmanPhotos = listOf(
        R.drawable.paso1,
        R.drawable.paso2,
        R.drawable.paso3,
        R.drawable.paso4,
        R.drawable.paso5,
        R.drawable.paso6,
        R.drawable.paso7
    )
    var result: Boolean = false
    lateinit var difficult: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = GameScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val bundle: Bundle? = intent.extras
        difficult = bundle?.getString("gameDifficult").toString()

        if (difficult == "Easy (Mundial 2022)") words = easy
        else if (difficult == "Medium (Mundial 2010)") words = medium
        else words = hard
        wordrandom = words[(words.indices).random()].uppercase()
        for (i in wordrandom.indices) {
            if (i.toString() =="/") wordMessage+= "/ "
            else wordMessage+= "_ "
        }
        wordMessage.dropLast(1)
        for (i in wordMessage.indices) {
            if (wordMessage[i] != ' ') {
                listOfWordMessage.add(wordMessage[i].toString())
            }

        }
        binding.wordTextView.text = wordMessage

        binding.buttonA.setOnClickListener(this)
        binding.buttonB.setOnClickListener(this)
        binding.buttonC.setOnClickListener(this)
        binding.buttonD.setOnClickListener(this)
        binding.buttonE.setOnClickListener(this)
        binding.buttonF.setOnClickListener(this)
        binding.buttonG.setOnClickListener(this)
        binding.buttonH.setOnClickListener(this)
        binding.buttonI.setOnClickListener(this)
        binding.buttonJ.setOnClickListener(this)
        binding.buttonK.setOnClickListener(this)
        binding.buttonL.setOnClickListener(this)
        binding.buttonM.setOnClickListener(this)
        binding.buttonN.setOnClickListener(this)
        binding.button.setOnClickListener(this)
        binding.buttonO.setOnClickListener(this)
        binding.buttonP.setOnClickListener(this)
        binding.buttonQ.setOnClickListener(this)
        binding.buttonR.setOnClickListener(this)
        binding.buttonS.setOnClickListener(this)
        binding.buttonT.setOnClickListener(this)
        binding.buttonU.setOnClickListener(this)
        binding.buttonV.setOnClickListener(this)
        binding.buttonW.setOnClickListener(this)
        binding.buttonX.setOnClickListener(this)
        binding.buttonY.setOnClickListener(this)
        binding.buttonZ.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        val button = p0 as Button
        val value = button.text.toString().uppercase()
        wordMessage = ""

        if (value !in wordrandom) {
            binding.image.setImageResource(hangmanPhotos[attempts])
            attempts++
        }

        for (i in wordrandom.indices) {
            if (wordrandom[i].toString() == value) {
                listOfWordMessage[i] = value
            }
        }

        for (i in listOfWordMessage.indices) {
            wordMessage += "${listOfWordMessage[i]} "
        }
        if ("_" !in wordMessage) result = true

        if (result || attempts == 7) {
            val intent = Intent(this, ActivityResult::class.java)
            intent.putExtra("winner", result)
            intent.putExtra("attempts", attempts)
            intent.putExtra("difficult", difficult)
            startActivity(intent)
        }

        wordMessage.dropLast(1)
        button.isEnabled = false
        button.alpha = 0.5F
        binding.wordTextView.text = wordMessage
    }
}


