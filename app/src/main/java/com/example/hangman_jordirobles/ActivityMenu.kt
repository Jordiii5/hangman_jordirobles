package com.example.hangman_jordirobles

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.hangman_jordirobles.databinding.MenuScreenBinding

class ActivityMenu : AppCompatActivity() {

    lateinit var binding: MenuScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MenuScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonPanelPlay.setOnClickListener {
            val intent = Intent(this, ActivityGame::class.java)
            intent.putExtra("gameDifficult", binding.spinner.selectedItem.toString())
            startActivity(intent)
        }
        binding.buttonPanelHelp.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            val view = layoutInflater.inflate(R.layout.dialoghelp,null)

            builder.setView(view)

            val dialog = builder.create()
            dialog.show()
        }
    }
}