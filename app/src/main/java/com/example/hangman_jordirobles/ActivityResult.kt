package com.example.hangman_jordirobles

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.hangman_jordirobles.databinding.ResultScreenBinding

class ActivityResult : AppCompatActivity() {
    lateinit var binding: ResultScreenBinding
    lateinit var difficult: String
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ResultScreenBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val bundle: Bundle? = intent.extras
        val winner = bundle?.getBoolean("winner")
        difficult = bundle?.getString("dificultat").toString()
        val attempts = bundle?.getInt("attempts")
        if (winner==true ) {
            binding.firstmessage.text = "CONGRATULATIONS!!"
            binding.secondmessage.text = "You have won after $attempts attempts"
        }
        else{
            binding.firstmessage.text = "YOU HAVE LOST!!"
            binding.secondmessage.text = "You have lose after $attempts attempts"
        }
        binding.button1.setOnClickListener {
            val intent = Intent(this, ActivityGame::class.java)
            intent.putExtra("difficult", difficult)
            startActivity(intent)
        }
        binding.button2.setOnClickListener {
            val intent = Intent(this, ActivityMenu::class.java)
            startActivity(intent)
        }
    }
}
